#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define MAXLEN 32


typedef struct _data_t {
    int id; 
    char name[MAXLEN];
} data_t;


// this memory is shared by all threads, notice how things can happen 
// differently each time!
int shared[10];
int shared_ndx = 0;
pthread_mutex_t mutex;

void *run(void *ptr)
{
    data_t *data = (data_t *) ptr;

    // don't change this line; this simulates non-deterministic work
    usleep(rand() % 100);

    // now after the loop finishes, we'll modify
    // the shared memory
    
    // BEGIN critical section
    pthread_mutex_lock(&mutex);

    printf("%s writing to shared[]\n", data->name);    
    shared[shared_ndx++] = data->id;
    fflush(stdout);

    pthread_mutex_unlock(&mutex);
    // END critical section
}


int main(void)
{
    // do not change this line
    srand(time(NULL));

    pthread_mutex_init(&mutex, NULL);

    puts("================================================================");
    puts("                          CHALLENGE");
    puts("================================================================");
    puts("\nMake the numbers print in order from least to greatest. Only\n"
         "accomplish this by synchronizing writes to shared[] in\n"
         "order. You must win *every* time you run the code. Not just get\n"
         "lucky."); 

    puts("----------------------------------------------------------------\n");

    puts("================================================================");
    puts("                          DEBUG INFO");
    puts("================================================================\n");

    for(int i = 0; i < 10; i++)
    {
        pthread_t thread = 0;

        // create data object to pass to thread (often, examples will
        // use a hack that passes an integer using the storage space
        // of a pointer to int, but not actually point at the int...
        // this is not a good way to explain it! you'll often use a
        // struct)
        //
        // note, that the data object is being allocated on the stack but
        // because it's on the stack in main() it's not going anywhere
        // until the program finishes. remember how long things that
        // are allocated on the stack live vs on the heap (w malloc(), etc)
        data_t *data = malloc(sizeof *data);
        data->id = i;
        snprintf(data->name, MAXLEN, "Thread#%d\n", i);

        // create & start the thread
        printf("&data=%p\n", data);
        pthread_create(&thread, NULL, run, data);
    }

    // hey, how does this work?
    size_t len = sizeof shared / sizeof *shared;

    // we need to wait until all the work is done before moving on;
    // we'll spin here (you won't need to use this spinning trick 
    // on your assessment, and you definitely shouldn't! Don't remove
    // this code while solving the challenge. It must stay.)
    while (shared_ndx != len); // spin
    pthread_mutex_lock(&mutex);
    pthread_mutex_unlock(&mutex);

    puts("================================================================");
    puts("                          NUMBERS");
    puts("================================================================\n");

    printf("The numbers are: ");
    

    // look at this: how does it work (the sizeof part)
    for(int i = 0; i < len; i++)
    {
        printf("%d ", shared[i]);
    }

    puts("");

    // do not change below this line
    // ------------------------------------------------------------------------
    
    int max  = shared[0];

    // how does this work?
    int i;
    for (i = 0; i < len; i++)
    {
        if (shared[i] < max) break;
        else
            max = shared[i];
    }

    if (i != len)
    {
        puts("You lose!");
    }
    else 
    {
        puts("You win!");
    }


    pthread_mutex_destroy(&mutex);
    exit(EXIT_SUCCESS);
}

